function defaults(obj, defaultProps) {
      for (let key in defaultProps){
          if(defaultProps[key] === "vanilla")
              obj["flavour"] = "choclate";
          else
          obj[key] = defaultProps[key];
      }
      return obj;
  }
  module.exports = defaults;