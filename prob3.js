function mapObject(obj, cb) {
    var key = Object.keys(obj);
    var arr = [];
    for(var i=0;i<key.length;i++){
        if( key[i]!=undefined ){
            arr.push(cb(key[i],obj[key[i]]));
        }
    }
    return arr;
}
module.exports=mapObject;